# We want redhat/centos machine
# FROM is the base container that you'll build on 
FROM centos:centos8.3.2011
# Pinning your version is good practice 

# RUN is used to install at build time
# We want to download httpd
RUN yum install -y httpd

# We want our own index file 
# COPY from index.html from local dir to /var/www/html in container
COPY index.html /var/www/html

# PORT for orchestration tool
    # Meta data is important for Docker compose and kubernetes

EXPOSE 80
EXPOSE 443

# ENTRYPOINT preffered when running executables 
ENTRYPOINT ["httpd", "-D", "FOREGROUND"]

