from flask import Flask
from flask_restful import Resource, Api

## Initiating instance of Flask App
app = Flask (__name__)
## Passing entire Flask App into initiating an API able object
api = Api(app)

class Product(Resource):
    def get(self):
        return {
            'products': ['Orange Muffin', 'Plain Old Muffin', 'Blueberry Muffin']
        }


class ProductPrice(Resource):
    def get(self):
        return {
            'products': {
                'Orange Muffin': 10,
                'Plain Old Muffin': 17,
                'Blueberry Muffin': 12
            }
        }
    

api.add_resource(Product, '/')
api.add_resource(ProductPrice, '/price')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)