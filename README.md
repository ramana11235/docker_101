# Docker 101

Docker is a tool which creates and manages containers.

## Main Commands

Running our first container 

```bash
docker run -d -p 80:80 docker/getting-started


#docker run [-options] owner_repo/image

    # -d detached/daemon (run in background)
    # -p port specify mapping of a port (80:80 - host:guest)
        # Host id is the physical machine in which you're running docker 
        # Guest is the app inside the container

docker ps
    # this tells u the running processes and the ids 

# To change the port on which the app runs (e.g. 7900)

docker run -dp 7900:80 docker/getting-started

# To kill the app running on 80

docker kill <PID>

    # <PID> obtained from docker ps


```




## Docker Hub

ToDo:
- What is docker hub?
- find the hello-world docker image 
- download and run it on port 80 
- Give the running container a name
  - Run another container with hello-world app and call it "Bazinga" and make it run on port 4501


```bash
docker pull hello-world

docker run hello-world

docker run -d -p 7900:80 -it hello-world

## can also have option -dp to COMBINE

docker run --name bazinga -d -p 4501:80 hello-world
```


## Installing Postgress through Docker 

- `docker run -e POSTGRES_PASSWORD=password -e POSTGRES_HOST_AUTH_METHOD=trust --name my_postgres -d postgres:latest`
  - `-e VARIABLE=value` allows for variable input 
  - `-d` lets it run in background 
  - `--name my_postgres` sets name for container
  - `postgres:latest` allows for specific versions

### Running commands inside the container 

- `docker exec -it my_postgres psql -U postgres -W `
  - `-U` is user
  - `-W` prompts for password
  - case matters

```sql
CREATE TABLE BOOKS (Title VARCHAR(64),  
Author VARCHAR(300));
```

- `\dt` to show databases

- `docker build -t real_httpd . `
  - builds your dockerfile with name real_httpd
- `docker run -itd --name httpd_server real_httpd`
  - run it in contained called httpd_server
- ` docker run real_httpd ` 
  - run container


## Dockerfile


```dockerfile
# PORT for orchestration tool
    # Meta data is important for Docker compose and kubernetes

EXPOSE 80
EXPOSE 443
```
  - This is setting metadata to tell the container/docker that there is an app running on these ports. It doesn't actually open the ports, as that's done by the actual applications


## Docker Compose

DOcker compose is to run multiple docker images in one 

## Docker Registriy 

Docker registr 